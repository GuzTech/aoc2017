My attempt at solving Advent of Code 2017 (https://adventofcode.com) using Rust.

Source code is in `src/bin/dayX` and can be run using:

```
cargo run --bin dayX
```

where you replace `X` with the day number.