/*
    Advent of Code 2017 - Day 3

    Part 1:
    There is a grid like the following:
    
    17  16  15  14  13
    18   5   4   3  12
    19   6   1   2  11
    20   7   8   9  10
    21  22  23---> ...

    First find the ring that the number is in. "1" is in ring 0, numbers between
    2 and 9 are in ring 1, etc.
    Each ring has four sides with an equal amount of numbers (except ring 0).
    These sides are 0 (right), 1 (up), 2 (left), and 3 (down).

    The main point that I see is that the distance between the position and the
    center is the same distance as that same position and the starting position
    of the side it is on.
    
    For example: 16 is in ring 2 and on side 1 (up). The starting position of
    side 1 is 13, so the distance between 16 and 1 is the same as 16 and 13,
    which is 3.

    Another example: 19 is in ring 2 and on side 2 (left). The starting position
    of side 2 is 17, so the distance between 19 and 1 (thus also 17) is 19-17=2.
 */

fn is_odd(i: i64) -> bool {
    i % 2 == 1
}

fn main() {
    let input: i64 = 25;//347991;
    let corrected_input = input - 1;

    let sqrt_num = (corrected_input as f64).sqrt().floor() as i64;
    let in_power_of_two = if is_odd(sqrt_num) {
        sqrt_num // This is the minimum
    } else {
        sqrt_num - 1
    };
    let minimum_ring = in_power_of_two / 2;

    let in_ring = if input == 1 {
        0
    } else {
        minimum_ring + 1
    };
    let in_between_values = (in_power_of_two * in_power_of_two, (in_power_of_two + 2) * (in_power_of_two + 2));
    let num_numbers_in_ring = in_between_values.1 - in_between_values.0;
    let num_numbers_per_side = num_numbers_in_ring / 4;

    let position_from_start_of_ring = input - in_between_values.0;
    let on_side = position_from_start_of_ring / num_numbers_per_side;
    let distance = position_from_start_of_ring - (num_numbers_per_side * on_side);

    println!("In ring {}", in_ring);
    println!("Between values {} and {}", in_between_values.0, in_between_values.1);
    println!("Number of numbers in ring: {}", num_numbers_in_ring);
    println!("Number of numbers per side: {}", num_numbers_per_side);
    println!("On side: {}", on_side);
    println!("\nAnswer: {}", distance);
}
